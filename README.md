# Pg barman
## Links
* [Barman Manual](http://docs.pgbarman.org/release/2.5/)
  * [Installation on Debian/Ubuntu using packages](http://docs.pgbarman.org/release/2.5/#installation-on-debianubuntu-using-packages)
  * [Scenario 1: Backup via streaming protocol](http://docs.pgbarman.org/release/2.5/#scenario-1-backup-via-streaming-protocol)
  * [Configuration](http://docs.pgbarman.org/release/2.5/#configuration)
* []()
* []()
* []()
* []()

## Trash
* Recovery Point Objective (RPO): maximum targeted period in which data might be lost from an IT service due to a major incident
* Recovery Time Objective (RTO): the targeted duration of time and a service level within which a business process must be restored after a disaster (or disruption) in order to avoid unacceptable consequences associated with a break in business continuity

### Install barman
```bash
curl https://dl.2ndquadrant.com/default/release/get/deb | bash
apt update
apt install barman
systemctl status barman
```

### Setup of a new server in Barman
```bash
# on pg - target server
sudo -u postgres createuser -s -P barman
vim /etc/postgresql/9.5/main/pg_hba.conf
# host   all         barman   10.185.12.52/32 md5
systemctl restart postgresql

# on barman server
## https://www.postgresql.org/docs/9.5/libpq-pgpass.html
sudo -u barman vim /var/lib/barman/.pgpass
# 10.185.12.53:5432:*:barman:barman
sudo -u barman psql -c 'SELECT version()' -U barman -h 10.185.12.53 postgres

########################################
# on pg - target server
# wal_level = 'replica'
# wal_level = 'hot_standby'  # For PostgreSQL versions older than 9.6

########################################
# PostgreSQL streaming connection
# on pg - target server
sudo -u postgres createuser -P --replication streaming_barman
vim /etc/postgresql/9.5/main/pg_hba.conf
# host   replication streaming_barman 10.185.12.52/32 md5
# max_wal_senders = 2
# max_replication_slots = 2
systemctl restart postgresql

# on barman server
## https://www.postgresql.org/docs/9.5/libpq-pgpass.html
sudo -u barman vim /var/lib/barman/.pgpass
# 10.185.12.53:5432:*:streaming_barman:barman
sudo -u barman psql -U streaming_barman -h 10.185.12.53 -c "IDENTIFY_SYSTEM" replication=1

########################################
# SSH connections
# on pg -target server
sudo -u postgres ssh-keygen -t ed25519 -a 100
cat /var/lib/postgresql/.ssh/id_ed25519.pub

# on barman server
sudo -u barman ssh-keygen -t ed25519 -a 100
cat /var/lib/barman/.ssh/id_ed25519.pub
echo "id_ed25519.pub from pg" >> /var/lib/barman/.ssh/authorized_keys
echo -e "Host *\n   StrictHostKeyChecking no\n   UserKnownHostsFile=/dev/null\n" > /var/lib/barman/.ssh/config
chmod 600 /var/lib/barman/.ssh/*
chmod 700 /var/lib/barman/.ssh
chown -R barman:barman /var/lib/barman/.ssh

# on pg - target server
echo "id_ed25519.pub from barman" >> /var/lib/postgresql/.ssh/authorized_keys
echo -e "Host *\n   StrictHostKeyChecking no\n   UserKnownHostsFile=/dev/null\n" > /var/lib/postgresql/.ssh/config
chmod 600 /var/lib/postgresql/.ssh/*
chmod 700 /var/lib/postgresql/.ssh
chown -R postgres:postgres /var/lib/postgresql/.ssh
```

### The server configuration file
Create /etc/barman.d/target.conf:
```
[postgres01]
description =  "PostgreSQL-01 server"
conninfo = host=10.185.12.54 user=barman dbname=postgres
backup_method = postgres
streaming_conninfo = host=10.185.12.54 user=streaming_barman
streaming_archiver = on
slot_name = barman
```

* barman_home : Keeping the default backup location determining where the backup will be saved.
* compression: WAL(write ahead logs) logs will be compressed, and base backups will use incremental data copying
* last_backup_maximum_age : The age of the last full backup for a PostgreSQL server should not be older than 3 days
* retention_policy: We want to be able to recover our database to any time of the last seven days
* minimum_redundancy: This ensures that we have at least 3 base backups and will make sure that we accidentally do not delete backups.


Example of /etc/barman.conf
```bash
[barman]
barman_home = /var/lib/barman

barman_user = barman
log_file = /var/log/barman/barman.log
compression = gzip
bandwidth_limit = 0
immediate_checkpoint = true
last_backup_maximum_age = 3 DAYS
retention_policy = RECOVERY WINDOW OF 7 days
minimum_redundancy = 3
```


### WAL archive: FAILED (please make sure WAL shipping is setup)
```bash
barman switch-xlog --force --archive postgres01
```

### Start backup
```bash
barman backup <target>
barman backup all
barman cron
```

### Recover
```bash
barman recover --remote-ssh-command "ssh postgres@10.185.12.55" postgres01 20190624T181722 /var/lib/postgresql/9.5/main
```

## Barman commands
* archive-wal
* backup
* check
* check-backup
* cron
* delete
* diagnose
* get-wal
* list-backup
* list-files
* list-server
* put-wal
* rebuild-xlogdb
* receive-wal
* recover
* show-backup
* show-server
* replication-status
* status
* switch-wal
* switch-xlog
* sync-info
* sync-backup
* sync-wals

